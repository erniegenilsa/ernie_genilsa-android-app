package com.example.genilsacontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import java.util.logging.Level.INFO
import java.util.logging.Logger

class SecondScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_screen)

        var guestInfo = intent.extras?.getStringArrayList("guestInfo")!!;

        var haveSymptoms = "NO";
        var haveContact = "NO";

        val rbSymYes = findViewById<RadioButton>(R.id.symptomYes);
        val rbSymNo = findViewById<RadioButton>(R.id.symptomNo);
        val symptoms =  findViewById<EditText>(R.id.symptoms);

        val contactYes = findViewById<RadioButton>(R.id.contactYes);
        val contactNo = findViewById<RadioButton>(R.id.contactNo);
        val contactHistory =  findViewById<EditText>(R.id.contactHistory);

        val next = findViewById<Button>(R.id.btnNext);

        next.setOnClickListener {
            if (rbSymYes.isChecked){
                haveSymptoms = "YES";
            }
            if (contactYes.isChecked){
                haveContact = "YES"
            }
            guestInfo.add(haveSymptoms);
            guestInfo.add(symptoms.text.toString());
            guestInfo.add(haveContact);
            guestInfo.add(contactHistory.text.toString());

            Log.i("mama", guestInfo.toString());
            val intent = Intent(this, ThirdScreen::class.java)
            intent.putExtra("guestInfo", guestInfo)
            startActivity(intent)

        }

    }
}
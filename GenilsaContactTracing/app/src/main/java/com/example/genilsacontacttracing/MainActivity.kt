package com.example.genilsacontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val next = findViewById<Button>(R.id.btnNext);
        val name = findViewById<EditText>(R.id.fullName);
        val number = findViewById<EditText>(R.id.number);
        val address = findViewById<EditText>(R.id.address);

        var info = ArrayList<String>();


        next.setOnClickListener {
            info.add(name.text.toString());
            info.add(number.text.toString());
            info.add(address.text.toString());
            val intent = Intent(this, SecondScreen::class.java)
            intent.putExtra("guestInfo", info)
            startActivity(intent)

        }

    }


}
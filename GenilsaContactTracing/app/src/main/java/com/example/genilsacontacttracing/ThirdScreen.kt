package com.example.genilsacontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class ThirdScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third_screen)

        var guestInfo = intent.extras?.getStringArrayList("guestInfo")!!;

        Log.i("mama", guestInfo.toString());
        val name = findViewById<TextView>(R.id.fullName);
        val number = findViewById<TextView>(R.id.contactNumber)
        val address = findViewById<TextView>(R.id.address)
        val haveSymptoms = findViewById<TextView>(R.id.haveSymptoms)
        val symptoms = findViewById<TextView>(R.id.symptoms)
        val haveContact = findViewById<TextView>(R.id.haveContact)
        val contactHistory = findViewById<TextView>(R.id.contactHistory)
        val done = findViewById<Button>(R.id.btnNext);

        name.text = name.text.toString() + guestInfo[0];
        number.text = number.text.toString() + guestInfo[1];
        address.text = address.text.toString() + guestInfo[2];
        haveSymptoms.text = haveSymptoms.text.toString() + guestInfo[3];
        symptoms.text = symptoms.text.toString() + guestInfo[4];
        haveContact.text = haveContact.text.toString() + guestInfo[5];
        contactHistory.text = contactHistory.text.toString() + guestInfo[6];

        done.setOnClickListener {
            startActivity(Intent(this, FourthScreen::class.java))
        }
    }
}